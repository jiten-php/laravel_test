$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
/* url in ajax left blank, it will take from route when requested url(browser) mathces string defined in routes */
$('form').submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var action_url = $(this).attr('action');
    console.log(action_url);
    $.ajax({
        type:'POST',
        url: "",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        dataType:'json',
        success: function(data) {
            if (data.error) {
                $.each(data.error,function(key, value){
                    if (key) {
                        $( "[name='" + key + "']" ).next().text( value );
                            setTimeout(function() {
                                $( "[name='" + key + "']" ).next().text("");
                            },5000);
                    }           
                });
            }
        },
        error: function(data){
            console.log(data);
        }
    });
});