<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use DB;


class RegisterController extends Controller
{
    /**
     * Display user registration form
     */ 
    public function addUser()
    {
        $countries= DB::table("countries")->get();
    	return view('crud-form',compact('countries'));
    }

    /**
     * Save user information into "users" Table
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeUser(Request $request)
    {
        $request->validate([
            'name'=>'required|string|min:3|max:20',
            'email'=>'required|email|unique:users',
            'phone'=>'required|numeric|digits_between:8,10',
            'country'=>'required',
            'state'=>'required',
            'city'=>'required',
            'pin'=>'required|numeric|digits_between:6,8',
            'password' => 'required|string|confirmed|min:6',
        ]);

        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->country = $request->country;
            $user->state = $request->state;
            $user->city = $request->city;
            $user->pin = $request->pin;
            $user->password = Hash::make($request->password);
            $user->save();

            return redirect()->route('add.user.form')->with('success', 'User registered successfully!');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
    
    /**
     * Fetch all list of states on behalf country_id 
     * from "states" table
     * @param country_id
     */
    public function getStates($country_id)
    {
        $states = DB::table("states")
                    ->where("country_id",$country_id)
                    ->pluck("name","id");
        return response()->json($states);
    }
 
    /**
     * Fetch all list of cities on behalf state_id 
     * from "cities" table
     * @param state_id
     */
    public function getCities($state_id)
    {
        $cities= DB::table("cities")
                    ->where("state_id",$state_id)
                    ->pluck("name","id");
        return response()->json($cities);
    }

    

}
