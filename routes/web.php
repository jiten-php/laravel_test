<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('add-user','RegisterController@addUser')->name('add.user.form');
Route::post('save-user','RegisterController@storeUser')->name('add.user');

Route::get('/country-list','RegisterController@getCountries');
Route::get('/state-list/{id}','RegisterController@getStates');
Route::get('/citie-list/{id}','RegisterController@getCities');