@extends('layout.registration')
@section('content')

<div class="container-fluid">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add User</h1>
	</div>
		
  <form method="post" id="user_form" action="{{ route('add.user') }}">
    @csrf
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputEmail4">Name</label>
      <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Name" value="{{ old('name') }}">
      @if($errors->has('name'))
                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
      @endif
    </div>

    <div class="form-group col-md-4">
      <label for="inputEmail4"> Email</label>
      <input type="text" name="email" class="form-control" id="inputEmail4" placeholder="email" value="{{ old('email') }}">
      @if($errors->has('email'))
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
        @endif
    </div>
    <div class="form-group col-md-4">
      <label for="inputPassword4">Phone</label>
      <input type="text" name="phone" class="form-control" id="inputPassword4" placeholder="phone" value="{{ old('phone') }}">
      @if($errors->has('phone'))
                <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
        @endif
    </div>
    <div class="form-group col-md-4">
      <label for="inputPassword4">Pin</label>
      <input type="text" name="pin" class="form-control" id="inputPassword4" placeholder="pin" value="{{ old('pin') }}">
      @if($errors->has('pin'))
                <div class="invalid-feedback">{{ $errors->first('pin') }}</div>
        @endif
    </div>

    <div class="form-group col-md-4">
      <label for="inputPassword4">Password</label>
      <input type="password" name="password" class="form-control" id="inputPassword4" placeholder="password" value="{{ old('password') }}">
      @if($errors->has('password'))
                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
        @endif
    </div>
    <div class="form-group col-md-4">
      <label for="password_confirmation">Confirm Password</label>
      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="password confirmation" value="{{ old('password_confirmation') }}">
    </div>

    <div class="form-group col-md-4">
      <label for="country">Country</label>
      <select id="country" name="country" class="form-control">
       <option value="">Select Country</option>
        @foreach ($countries as $country) 
            <option value="{{$country->id}}">
             {{$country->name}}
            </option>
        @endforeach
      </select>
      @if($errors->has('country'))
              <div class="invalid-feedback">{{ $errors->first('country') }}</div>
        @endif
    </div>

    <div class="form-group col-md-4">
      <label for="state">State</label>
      <select id="state" name="state" class="form-control">
        <option value="">Select State</option>
      </select>
      @if($errors->has('state'))
              <div class="invalid-feedback">{{ $errors->first('state') }}</div>
        @endif
    </div>

    <div class="form-group col-md-4">
      <label for="city">City</label>
      <select id="city" name="city" class="form-control">
        <option value="">Select City</option>
      </select>
      @if($errors->has('city'))
              <div class="invalid-feedback">{{ $errors->first('city') }}</div>
        @endif
    </div>

  </div>

  <button type="submit" class="btn btn-primary">Save</button>
</form>
</div>

<script type="text/javascript">
// Get state name list on select country 
$('#country').change(function(){
    var country_id = $(this).val();
    if (country_id) {
      $.ajax({
         type:"get",
         url:"{{url('/state-list')}}/"+country_id,
         success:function(response)
         {       
              if(response){
                  $("#state").empty();
                  $("#city").empty();
                  $("#state").append('<option>Select State</option>');
                  $.each(response,function(key,value){
                      $("#state").append('<option value="'+key+'">'+value+'</option>');
                  });
              }
         }
   
      });
    }
});
// Get city name list on select state
$('#state').change(function(){
    var state_id = $(this).val();
    if (state_id) {
      $.ajax({
         type:"get",
         url:"{{url('/citie-list')}}/"+state_id, 
         success:function(response)
         {       
              if(response){
                  $("#city").empty();
                  $("#city").append('<option>Select City</option>');
                  $.each(response,function(key,value){
                      $("#city").append('<option value="'+key+'">'+value+'</option>');
                  });
              }
         }
   
      });
    }
}); 


</script>

@endsection